FROM adoptopenjdk/openjdk11:ubi
COPY /target/*.jar /usr/app/cadastro-java.jar
ENV profile=dev
EXPOSE 8080
ENTRYPOINT ["java", "-Dspring.profiles.active=${profile}","-Duser.timezone=America/Sao_Paulo", "-jar", "/usr/app/cadastro-java.jar"]

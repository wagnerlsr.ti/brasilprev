package br.com.brasilprev.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@AllArgsConstructor
public class Errors {

  @JsonProperty("timestamp")
  private LocalDateTime timestamp = null;

  @JsonProperty("status")
  private Integer status = null;

  @JsonProperty("error")
  private String error = null;

  @JsonProperty("path")
  private String path = null;

  @JsonProperty("transactionId")
  private String transactionId = null;

  @JsonProperty("errors")
  @Valid
  private List<ErrorErrors> errors = new ArrayList<ErrorErrors>();

  public Errors timestamp(LocalDateTime timestamp) {
    this.timestamp = timestamp;
    return this;
  }

  /**
   * Timestamp de quando ocorreu o erro
   * @return timestamp
  **/
  @ApiModelProperty(required = true, value = "Timestamp de quando ocorreu o erro")
  @NotNull

  @Valid

  public LocalDateTime getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(LocalDateTime timestamp) {
    this.timestamp = timestamp;
  }

  public Errors status(Integer status) {
    this.status = status;
    return this;
  }

  /**
   * Código de status HTTP do erro
   * @return status
  **/
  @ApiModelProperty(example = "400 | 401 | 403 | 404 | 500", required = true, value = "Código de status HTTP do erro")
  @NotNull

  @Valid

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public Errors error(String error) {
    this.error = error;
    return this;
  }

  /**
   * Nome do erro HTTP
   * @return error
  **/
  @ApiModelProperty(example = "Bad Request | Unauthorized | Forbidden | Not Found | Internal Server Error", required = true, value = "Nome do erro HTTP")
  @NotNull


  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public Errors path(String path) {
    this.path = path;
    return this;
  }

  /**
   * Path da API que foi chamada
   * @return path
  **/
  @ApiModelProperty(example = "/accounts/123456/receipts", required = true, value = "Path da API que foi chamada")
  @NotNull


  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public Errors transactionId(String transactionId) {
    this.transactionId = transactionId;
    return this;
  }

  /**
   * NSU da transação da chamada
   * @return transactionId
  **/
  @ApiModelProperty(example = "e9ef6d22-7f46-426b-8121-417e1714ad6d", required = true, value = "NSU da transação da chamada")
  @NotNull


  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public Errors errors(List<ErrorErrors> errors) {
    this.errors = errors;
    return this;
  }

  public Errors addErrorsItem(ErrorErrors errorsItem) {
    this.errors.add(errorsItem);
    return this;
  }

  /**
   * Get errors
   * @return errors
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public List<ErrorErrors> getErrors() {
    return errors;
  }

  public void setErrors(List<ErrorErrors> errors) {
    this.errors = errors;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Errors error = (Errors) o;
    return Objects.equals(this.timestamp, error.timestamp) &&
        Objects.equals(this.status, error.status) &&
        Objects.equals(this.error, error.error) &&
        Objects.equals(this.path, error.path) &&
        Objects.equals(this.transactionId, error.transactionId) &&
        Objects.equals(this.errors, error.errors);
  }

  @Override
  public int hashCode() {
    return Objects.hash(timestamp, status, error, path, transactionId, errors);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Error {\n");
    
    sb.append("    timestamp: ").append(toIndentedString(timestamp)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    error: ").append(toIndentedString(error)).append("\n");
    sb.append("    path: ").append(toIndentedString(path)).append("\n");
    sb.append("    transactionId: ").append(toIndentedString(transactionId)).append("\n");
    sb.append("    errors: ").append(toIndentedString(errors)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


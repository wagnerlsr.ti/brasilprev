package br.com.brasilprev.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;


@AllArgsConstructor
public class ErrorErrors   {
  @JsonProperty("uniqueId")
  private String uniqueId = null;

  @JsonProperty("informationCode")
  private String informationCode = null;

  @JsonProperty("message")
  private String message = null;

  public ErrorErrors uniqueId(String uniqueId) {
    this.uniqueId = uniqueId;
    return this;
  }

  /**
   * NSU do erro
   * @return uniqueId
  **/
  @ApiModelProperty(example = "fd9b9a41-85cf-4654-aa63-6c9c8954ef5b", value = "NSU do erro")


  public String getUniqueId() {
    return uniqueId;
  }

  public void setUniqueId(String uniqueId) {
    this.uniqueId = uniqueId;
  }

  public ErrorErrors informationCode(String informationCode) {
    this.informationCode = informationCode;
    return this;
  }

  /**
   * Código customizado do erro
   * @return informationCode
  **/
  @ApiModelProperty(example = "18", value = "Código customizado do erro")


  public String getInformationCode() {
    return informationCode;
  }

  public void setInformationCode(String informationCode) {
    this.informationCode = informationCode;
  }

  public ErrorErrors message(String message) {
    this.message = message;
    return this;
  }

  /**
   * Mensagem customizada detalhando o erro
   * @return message
  **/
  @ApiModelProperty(example = "Parametros invalidos", value = "Mensagem customizada detalhando o erro")


  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ErrorErrors errorErrors = (ErrorErrors) o;
    return Objects.equals(this.uniqueId, errorErrors.uniqueId) &&
        Objects.equals(this.informationCode, errorErrors.informationCode) &&
        Objects.equals(this.message, errorErrors.message);
  }

  @Override
  public int hashCode() {
    return Objects.hash(uniqueId, informationCode, message);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ErrorErrors {\n");
    
    sb.append("    uniqueId: ").append(toIndentedString(uniqueId)).append("\n");
    sb.append("    informationCode: ").append(toIndentedString(informationCode)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


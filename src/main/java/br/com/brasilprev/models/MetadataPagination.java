package br.com.brasilprev.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * MetadataPagination
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-08-23T01:43:19.948Z")




public class MetadataPagination   {
  @JsonProperty("offset")
  private Integer offset = null;

  @JsonProperty("limit")
  private Integer limit = null;

  @JsonProperty("totalRecords")
  private Integer totalRecords = null;

  @JsonProperty("returnedRecords")
  private Integer returnedRecords = null;

  @JsonProperty("sort")
  private String sort = null;

  public MetadataPagination offset(Integer offset) {
    this.offset = offset;
    return this;
  }

  /**
   * Posição do primeiro registro a ser retornado
   * @return offset
  **/
  @ApiModelProperty(example = "0", required = true, value = "Posição do primeiro registro a ser retornado")
  @NotNull


  public Integer getOffset() {
    return offset;
  }

  public void setOffset(Integer offset) {
    this.offset = offset;
  }

  public MetadataPagination limit(Integer limit) {
    this.limit = limit;
    return this;
  }

  /**
   * Número de registros a serem retornados
   * @return limit
  **/
  @ApiModelProperty(example = "10", required = true, value = "Número de registros a serem retornados")
  @NotNull


  public Integer getLimit() {
    return limit;
  }

  public void setLimit(Integer limit) {
    this.limit = limit;
  }

  public MetadataPagination totalRecords(Integer totalRecords) {
    this.totalRecords = totalRecords;
    return this;
  }

  /**
   * Total de registros existentes
   * @return totalRecords
  **/
  @ApiModelProperty(example = "150", required = true, value = "Total de registros existentes")
  @NotNull


  public Integer getTotalRecords() {
    return totalRecords;
  }

  public void setTotalRecords(Integer totalRecords) {
    this.totalRecords = totalRecords;
  }

  public MetadataPagination returnedRecords(Integer returnedRecords) {
    this.returnedRecords = returnedRecords;
    return this;
  }

  /**
   * Total de registros retornados na resposta
   * @return returnedRecords
  **/
  @ApiModelProperty(example = "10", required = true, value = "Total de registros retornados na resposta")
  @NotNull


  public Integer getReturnedRecords() {
    return returnedRecords;
  }

  public void setReturnedRecords(Integer returnedRecords) {
    this.returnedRecords = returnedRecords;
  }

  public MetadataPagination sort(String sort) {
    this.sort = sort;
    return this;
  }

  /**
   * Campos que serão utilizados para a ordenação
   * @return sort
  **/
  @ApiModelProperty(example = "name:asc", required = true, value = "Campos que serão utilizados para a ordenação")
  @NotNull


  public String getSort() {
    return sort;
  }

  public void setSort(String sort) {
    this.sort = sort;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MetadataPagination metadataPagination = (MetadataPagination) o;
    return Objects.equals(this.offset, metadataPagination.offset) &&
        Objects.equals(this.limit, metadataPagination.limit) &&
        Objects.equals(this.totalRecords, metadataPagination.totalRecords) &&
        Objects.equals(this.returnedRecords, metadataPagination.returnedRecords) &&
        Objects.equals(this.sort, metadataPagination.sort);
  }

  @Override
  public int hashCode() {
    return Objects.hash(offset, limit, totalRecords, returnedRecords, sort);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MetadataPagination {\n");
    
    sb.append("    offset: ").append(toIndentedString(offset)).append("\n");
    sb.append("    limit: ").append(toIndentedString(limit)).append("\n");
    sb.append("    totalRecords: ").append(toIndentedString(totalRecords)).append("\n");
    sb.append("    returnedRecords: ").append(toIndentedString(returnedRecords)).append("\n");
    sb.append("    sort: ").append(toIndentedString(sort)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package br.com.brasilprev.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;


@Entity
public class Customer   {

  @Id
  @GeneratedValue
  @JsonProperty("id")
  private Integer id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("cpf")
  private String cpf = null;

  @JsonProperty("address")
  private String address = null;

  public Customer id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Identificador do cliente
   * @return id
  **/
  @ApiModelProperty(example = "1", value = "Identificador do cliente")


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Customer name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Nome do cliente
   * @return name
  **/
  @ApiModelProperty(example = "Wagner Rodrigues", value = "Nome do cliente")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Customer cpf(String cpf) {
    this.cpf = cpf;
    return this;
  }

  /**
   * CPF do cliente
   * @return cpf
  **/
  @ApiModelProperty(example = "12345678901", value = "CPF do cliente")


  public String getCpf() {
    return cpf;
  }

  public void setCpf(String cpf) {
    this.cpf = cpf;
  }

  public Customer address(String address) {
    this.address = address;
    return this;
  }

  /**
   * Endereço do cliente
   * @return address
  **/
  @ApiModelProperty(example = "Avenida Paulista, 1000", value = "Endereço do cliente")


  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Customer customer = (Customer) o;
    return Objects.equals(this.id, customer.id) &&
        Objects.equals(this.name, customer.name) &&
        Objects.equals(this.cpf, customer.cpf) &&
        Objects.equals(this.address, customer.address);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, cpf, address);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Customer {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    cpf: ").append(toIndentedString(cpf)).append("\n");
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


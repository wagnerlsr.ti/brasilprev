package br.com.brasilprev.repositories;

import br.com.brasilprev.models.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CustomersRepository extends PagingAndSortingRepository<Customer, Integer> {



}

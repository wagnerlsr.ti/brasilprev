package br.com.brasilprev.configs;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;


@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Value("${project.version}")
	private String version;

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).useDefaultResponseMessages(false)				
				.select()
				.apis(RequestHandlerSelectors.basePackage("br.com.brasilprev"))
				.paths(PathSelectors.any())
				.build()
				.securityContexts(Lists.newArrayList(securityContext()))
				.securitySchemes(apiKey())
				.forCodeGeneration(true)
				.apiInfo(apiInfo())
				.tags(new Tag("Cadastro de clientes.", ""));
	}
	
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("Brasilprev - Desafio Java")
				.description("API para exposição de recursos de clientes.")
				.version(this.version)
				.license("Apache License Version 2.0")
				.licenseUrl("https://www.apache.com/licenses/LICENSE-2.0")
				.build();
	}

	@Bean
	SecurityContext securityContext() {
		return SecurityContext.builder()
				.securityReferences(defaultAuth())
				.forPaths(PathSelectors.any())
				.build();
	}

	List<SecurityReference> defaultAuth() {
		AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		authorizationScopes[0] = authorizationScope;
		return Lists.newArrayList(
				new SecurityReference("JWT",  authorizationScopes));
	}


	private List<ApiKey> apiKey() {
		
		ApiKey key1 = new ApiKey("JWT", HttpHeaders.AUTHORIZATION, ApiKeyVehicle.HEADER.getValue());

		return Lists.newArrayList(key1);
	}

}

package br.com.brasilprev.services;

import br.com.brasilprev.models.Customer;

import java.util.List;


public interface CustomersService {

    Customer createCustomer(Customer customer);

    Customer updateCustomer(Customer customer);

    Customer findCustomer(Integer id);

    void deleteCustomer(Integer id);

    List<Customer> findAllCustomer();

}

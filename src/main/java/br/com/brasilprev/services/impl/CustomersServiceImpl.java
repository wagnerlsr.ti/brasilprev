package br.com.brasilprev.services.impl;

import br.com.brasilprev.exceptions.BadRequestException;
import br.com.brasilprev.exceptions.NotFoundException;
import br.com.brasilprev.models.Customer;
import br.com.brasilprev.repositories.CustomersRepository;
import br.com.brasilprev.services.CustomersService;
import com.google.common.collect.Iterators;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Service
@RequiredArgsConstructor
public class CustomersServiceImpl implements CustomersService {

    private final CustomersRepository customersRepository;


    @Override
    public Customer createCustomer(Customer customer) {

        if ( customersRepository.existsById(customer.getId()) )
            throw new BadRequestException("Cliente já cadastrado.");

        return customersRepository.save(customer);

    }

    @Override
    public Customer updateCustomer(Customer newCustomer) {

        if ( newCustomer == null || newCustomer.getId() <= 0 )
            throw new BadRequestException("Cliente invalido.");

        Customer customer = findCustomer(newCustomer.getId());

        if ( customer == null )
            throw new NotFoundException("Cliente não localizado.");

        customer.setName(newCustomer.getName());
        customer.setCpf(newCustomer.getCpf());
        customer.setAddress(newCustomer.getAddress());

        return customersRepository.save(customer);

    }

    @Override
    public Customer findCustomer(Integer id) {

        return customersRepository.findById(id).orElse(null);

    }

    @Override
    public void deleteCustomer(Integer id) {

        customersRepository.deleteById(id);

    }

    @Override
    public List<Customer> findAllCustomer() {

        return StreamSupport.stream(customersRepository.findAll().spliterator(), false).collect(Collectors.toList());

    }

}

package br.com.brasilprev.presenters;

import br.com.brasilprev.models.Customer;
import br.com.brasilprev.models.Metadata;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Builder
public class CustomersPresenter   {
  @JsonProperty("data")
  @Valid
  private List<Customer> data = null;

  @JsonProperty("metadata")
  private Metadata metadata = null;

  public CustomersPresenter data(List<Customer> data) {
    this.data = data;
    return this;
  }

  public CustomersPresenter addDataItem(Customer dataItem) {
    if (this.data == null) {
      this.data = new ArrayList<Customer>();
    }
    this.data.add(dataItem);
    return this;
  }

  /**
   * Get data
   * @return data
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Customer> getData() {
    return data;
  }

  public void setData(List<Customer> data) {
    this.data = data;
  }

  public CustomersPresenter metadata(Metadata metadata) {
    this.metadata = metadata;
    return this;
  }

  /**
   * Get metadata
   * @return metadata
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Metadata getMetadata() {
    return metadata;
  }

  public void setMetadata(Metadata metadata) {
    this.metadata = metadata;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CustomersPresenter customersPresenter = (CustomersPresenter) o;
    return Objects.equals(this.data, customersPresenter.data) &&
        Objects.equals(this.metadata, customersPresenter.metadata);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data, metadata);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CustomersPresenter {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("    metadata: ").append(toIndentedString(metadata)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


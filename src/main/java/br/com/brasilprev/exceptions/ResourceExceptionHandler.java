package br.com.brasilprev.exceptions;

import br.com.brasilprev.models.ErrorErrors;
import br.com.brasilprev.models.Errors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@RestControllerAdvice
public class ResourceExceptionHandler extends ResponseEntityExceptionHandler {
	
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity notFoundException(NotFoundException ex, HttpServletRequest request) {

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                this.errorExceptions(ex.getMessage(), HttpStatus.NOT_FOUND, request, 404, null, ex.getMessage()));

    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity badRequestException(BadRequestException ex, HttpServletRequest request) {

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                this.errorExceptions(ex.getMessage(), HttpStatus.BAD_REQUEST, request, ex.getCodeErro(), null, ex.getMessage()));

    }

    private Errors errorExceptions(String message, HttpStatus status,
                                   HttpServletRequest request,
                                   Integer errorCode, Object args, String messageDetail) {

        var xTransactionId = request.getHeader("x-transaction-id")!= null ? request.getHeader("x-transaction-id"):UUID.randomUUID().toString();
        List<ErrorErrors> errors = new ArrayList<>();

        errors.add(new ErrorErrors(xTransactionId, errorCode.toString(), messageDetail));

        return new Errors(LocalDateTime.now(), status.value(), status.getReasonPhrase(), message, request.getContextPath(), errors);
        
    }
    
    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity dateTimeParseException(RuntimeException ex, HttpServletRequest request) {

    	var xTransactionId = request.getHeader("x-transaction-id");

    	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                this.errorExceptions("Não definido", HttpStatus.INTERNAL_SERVER_ERROR, request, 0, null, "Não definido"));

    }

}

package br.com.brasilprev.exceptions;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BadRequestException extends RuntimeException implements HandlerExceptionResolver {

	private static final long serialVersionUID = 1L;

	Integer codeErro = 0;
	
    public BadRequestException() {
    }

    public BadRequestException(Integer codeErro) {    	
    	super(codeErro.toString());
    	this.codeErro = codeErro;
    }
    
    public BadRequestException(String message) {
        super(message);                
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);      
    }
    
    public BadRequestException(Integer codeErro, Throwable cause) {
        super(codeErro.toString(), cause);
        this.codeErro = codeErro;
    }

    public BadRequestException(Throwable cause) {
        super(cause);
    }

    public BadRequestException(Integer codeErro, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(codeErro.toString(), cause, enableSuppression, writableStackTrace);
        this.codeErro =codeErro;
    }

    public Integer getCodeErro() {
		return codeErro;
	}

	@Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        return null;
    }

}

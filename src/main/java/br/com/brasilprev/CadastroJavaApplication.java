package br.com.brasilprev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = {"br.com.brasilprev"})
public class CadastroJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(CadastroJavaApplication.class, args);
    }

}

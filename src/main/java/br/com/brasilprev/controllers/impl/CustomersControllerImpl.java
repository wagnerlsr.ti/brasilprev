package br.com.brasilprev.controllers.impl;

import br.com.brasilprev.controllers.CustomersController;
import br.com.brasilprev.exceptions.NotFoundException;
import br.com.brasilprev.models.Customer;
import br.com.brasilprev.presenters.CustomerPresenter;
import br.com.brasilprev.presenters.CustomersPresenter;
import br.com.brasilprev.services.CustomersService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;


@Validated
@RestController
@RequiredArgsConstructor
public class CustomersControllerImpl implements CustomersController {

    private static final Logger log = LoggerFactory.getLogger(CustomersController.class);

    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;
    private final CustomersService customersService;


    public ResponseEntity<CustomersPresenter> customersGet() {

        var customers = customersService.findAllCustomer();

        return new ResponseEntity<CustomersPresenter>(CustomersPresenter.builder().data(customers).build(), HttpStatus.OK);

    }

    public void customersIdDelete(@PathVariable("id") Integer id) {

        try {
            customersService.deleteCustomer(id);
        } catch (Exception e) {
            throw new NotFoundException("Cliente inexistente.");
        }
    }

    public ResponseEntity<CustomerPresenter> customersIdGet(@PathVariable("id") Integer id) {

        Customer customer = customersService.findCustomer(id);

        if ( customer == null )
            throw new NotFoundException("Cliente não localizado.");

        return new ResponseEntity<CustomerPresenter>(CustomerPresenter.builder().data(customer).build(), HttpStatus.OK);

    }

    public ResponseEntity<CustomerPresenter> customersPost(@Valid @RequestBody Customer body) {

        Customer customer = customersService.createCustomer(body);

        return new ResponseEntity<CustomerPresenter>(CustomerPresenter.builder().data(customer).build(), HttpStatus.OK);

    }

    public ResponseEntity<CustomerPresenter> customersPut(@Valid @RequestBody Customer body) {

        Customer customer = customersService.updateCustomer(body);

        return new ResponseEntity<CustomerPresenter>(CustomerPresenter.builder().data(customer).build(), HttpStatus.OK);

    }

}

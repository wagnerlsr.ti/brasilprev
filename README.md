# Brasilprev - Desafio Java
 
Cadastro de clientes :
* Listagem de clientes
* Cadastro de clientes
* Alteração de clientes
* Exclusão de clientes


## Deployment

Servidor AWS:

 [Brasilprev cadastro](http://54.209.241.154:8080/cadastro-java/v1/swagger-ui.html#/)

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* H2

## Docker

* Obter imagem: docker pull wagnerlsr/brasilprev
* Executar: docker run -p 8080:8080 wagnerlsr/brasilprev
## Versionamento

Git [Lab](https://gitlab.com/wagnerlsr.ti/brasilprev) para versionamento. Para versões disponíveis, veja [tags cadastro]

* Wagner Rodrigues 
* wagnerlsr.ti@gmail.com
* www.linkedin.com/in/wagnerlsr
* (11) 9.5559-5999
